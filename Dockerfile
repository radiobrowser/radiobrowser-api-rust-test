FROM alpine:3.18 as builder
ADD . /usr/src/actixtest
WORKDIR /usr/src/actixtest
RUN apk update
RUN apk add rustup gcc g++
RUN rustup-init -y
ENV PATH="/root/.cargo/bin:$PATH"
RUN cargo build --release

FROM alpine:3.18
COPY --from=builder /usr/src/actixtest/target/release/actixtest /usr/local/bin
RUN addgroup -S radiobrowser && \
 adduser -S -G radiobrowser radiobrowser && \
 apk add libgcc curl
USER radiobrowser:radiobrowser
EXPOSE 8080
ENTRYPOINT [ "actixtest" ]
CMD [ "--help" ]