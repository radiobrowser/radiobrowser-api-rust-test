CREATE TABLE station (
  id             SERIAL PRIMARY KEY,
  stationuuid    CHAR(36) NOT NULL,
  changeuuid     CHAR(36) NOT NULL,
  name           TEXT NOT NULL,
  url            TEXT NOT NULL,
  homepage       TEXT NOT NULL,
  favicon        TEXT NOT NULL,
  countrycode    TEXT NOT NULL,
  state          TEXT NOT NULL,
  language       TEXT NOT NULL,
  tags           TEXT NOT NULL,
  geo_lat        DOUBLE PRECISION NULL,
  geo_long       DOUBLE PRECISION NULL
);

CREATE TABLE stationhistory (
  id             SERIAL PRIMARY KEY,
  stationuuid    CHAR(36) NOT NULL,
  changeuuid     CHAR(36) NOT NULL,
  name           TEXT NOT NULL,
  url            TEXT NOT NULL,
  homepage       TEXT NOT NULL,
  favicon        TEXT NOT NULL,
  countrycode    TEXT NOT NULL,
  state          TEXT NOT NULL,
  language       TEXT NOT NULL,
  tags           TEXT NOT NULL,
  geo_lat        DOUBLE PRECISION NULL,
  geo_long       DOUBLE PRECISION NULL
);

CREATE TABLE remoteserver (
  id             SERIAL PRIMARY KEY,
  name           TEXT NOT NULL,
  last_station_uuid CHAR(36) NOT NULL
);

CREATE TABLE stationclick (
  id             SERIAL PRIMARY KEY,
  ip             TEXT NOT NULL,
  clicktime      TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  stationuuid    CHAR(36) NOT NULL,
  clickuuid      CHAR(36) NOT NULL
);