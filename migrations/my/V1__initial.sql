CREATE TABLE station (
  id             MEDIUMINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  stationuuid    CHAR(36) NOT NULL,
  changeuuid     CHAR(36) NOT NULL,
  name           TEXT NOT NULL,
  url            TEXT NOT NULL,
  homepage       TEXT NOT NULL,
  favicon        TEXT NOT NULL,
  countrycode    TEXT NOT NULL,
  state          TEXT NOT NULL,
  language       TEXT NOT NULL,
  tags           TEXT NOT NULL,
  geo_lat        DOUBLE PRECISION NULL,
  geo_long       DOUBLE PRECISION NULL
);

CREATE TABLE stationhistory (
  id             MEDIUMINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  stationuuid    CHAR(36) NOT NULL,
  changeuuid     CHAR(36) NOT NULL,
  name           TEXT NOT NULL,
  url            TEXT NOT NULL,
  homepage       TEXT NOT NULL,
  favicon        TEXT NOT NULL,
  countrycode    TEXT NOT NULL,
  state          TEXT NOT NULL,
  language       TEXT NOT NULL,
  tags           TEXT NOT NULL,
  geo_lat        DOUBLE PRECISION NULL,
  geo_long       DOUBLE PRECISION NULL
);

CREATE TABLE remoteserver (
  id             MEDIUMINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name           TEXT NOT NULL,
  last_station_uuid CHAR(36) NOT NULL
);

CREATE TABLE stationclick (
  id             MEDIUMINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  ip             TEXT NOT NULL,
  clicktime      DATETIME NOT NULL,
  stationuuid    CHAR(36) NOT NULL,
  clickuuid      CHAR(36) NOT NULL
);
