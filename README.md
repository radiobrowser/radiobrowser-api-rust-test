# Radiobrowser api
## Test
```bash
$ export DATABASE=mysql://radiouser:12345678@localhost/radio
# OR
$ export DATABASE=postgres://postgres:12345678@localhost/radio
# OR
$ export DATABASE=mongodb://root:12345678@localhost/radio?authSource=admin
RUST_LOG=debug cargo run --release -- --database  run
# start database POSTGRES
$ docker run -p 5432:5432 -e POSTGRES_PASSWORD=12345678 -e POSTGRES_DB=radio postgres
# OR MYSQL
$ docker run -p 3306:3306 -e MYSQL_RANDOM_ROOT_PASSWORD=true -e MYSQL_USER=radiouser -e MYSQL_PASSWORD=12345678 -e MYSQL_DATABASE=radio mysql --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
# OR MONGODB
$ docker run -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=root -e MONGO_INITDB_ROOT_PASSWORD=12345678 mongo
# add migrations
$ RUST_LOG=debug cargo run -- migrate
# Optional: TLS
$ openssl req -x509 -newkey rsa:4096 -nodes -keyout key.pem -out cert.pem -days 365 -subj '/CN=localhost'
# run test
$ RUST_LOG=debug cargo run -- run
# run test with TLS
$ RUST_LOG=debug cargo run -- run --tls --tls-cert cert.pem --tls-key key.pem
```

## Testdata
```bash
# Insert some stations
$ curl -X POST -H "Content-Type: application/json" -d '{"name":"Station 1","url":"https://station1.example.com"}' http://localhost:8080/json/add
$ curl -X POST -H "Content-Type: application/json" -d '{"name":"Station 2","url":"https://station2.example.com"}' http://localhost:8080/json/add
$ curl -X POST -H "Content-Type: application/json" -d '{"name":"Station 3","url":"https://station3.example.com"}' http://localhost:8080/json/add
```

## Stresstest
```bash
# start in release mode
$ RUST_LOG=debug cargo run --release -- run
# install wrk test tool (ubuntu)
$ apt install wrk
# run wrk for 10s
$ wrk http://localhost:8080/json/stations
Running 10s test @ http://localhost:8080/json/stations
  2 threads and 10 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    87.68us   56.33us   5.27ms   99.81%
    Req/Sec    57.13k     2.82k   68.20k    60.20%
  1142631 requests in 10.10s, 268.07MB read
Requests/sec: 113141.04
Transfer/sec:     26.54MB
```