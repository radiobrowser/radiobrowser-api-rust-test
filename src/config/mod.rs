use clap::{Parser, Subcommand};

/// Simple program to greet a person
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Cli {
    /// Database url
    #[clap(short, long, value_parser, env)]
    pub database: String,

    /// Number of parallel database connections kept
    #[clap(long, value_parser, default_value_t = 10, env)]
    pub database_pool_size: usize,

    /// Cache server url (redis)
    #[clap(short, long, value_parser, env)]
    pub cacheserver: Option<String>,

    /// Cache server entry ttl in seconds
    #[clap(long, value_parser, env, default_value_t = 60)]
    pub cacheserver_ttl: u32,

    #[clap(subcommand)]
    pub command: Commands,
}

#[derive(Debug, Subcommand)]
pub enum Commands {
    /// run migrations
    Migrate {},
    /// Import from other servers
    Pull {},
    /// Run server
    Run {
        /// HTTP port
        #[clap(short = 'l', long, env, value_parser, default_value_t = 8080)]
        listen_port: usize,

        /// HTTP listen ip
        #[clap(short = 'p', long, env, value_parser, default_value = "127.0.0.1")]
        listen_ip: String,

        /// Enable tls
        #[clap(short, long, env, value_parser, default_value_t = false)]
        tls: bool,

        /// TLS private key file path
        #[clap(long, env, value_parser, default_value = "cert.pem")]
        tls_cert: String,

        /// TLS certificate file path
        #[clap(long, env, value_parser, default_value = "key.pem")]
        tls_key: String,

        /// TLS port
        #[clap(long, env, value_parser, default_value_t = 8443)]
        tls_port: usize,

        /// Enable request logging
        #[clap(short = 'o', long, env, value_parser, default_value_t = false)]
        enable_logger: bool,

        /// Enable http response compression
        #[clap(short = 'c', long, env, value_parser, default_value_t = false)]
        enable_compress: bool,
    },
}
