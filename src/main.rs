mod config;
mod db;
mod pull;
mod web;

use clap::Parser;
use config::{Cli, Commands};
use db::DbAbstraction;
use log::{error, info};
use web::cache::CacheAbstraction;

fn run(
    database: String,
    database_pool_size: usize,
    tls_key: String,
    tls_cert: String,
    tls: bool,
    listen_ip: String,
    listen_port: usize,
    tls_port: usize,
    enable_logger: bool,
    enable_compress: bool,
    cacheserver: Option<String>,
    cache_ttl: u32,
) -> std::io::Result<()> {
    tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()?
        .block_on(async {
            let db: DbAbstraction = match DbAbstraction::new(database, database_pool_size).await {
                Ok(db) => db,
                Err(err) => {
                    panic!("DB connection could not be opened {}", err);
                }
            };

            let cache = match CacheAbstraction::new(cacheserver, cache_ttl) {
                Ok(cache) => cache,
                Err(err) => {
                    panic!("Cache load error: {}", err);
                }
            };

            if tls {
                web::run_tls(
                    db,
                    listen_ip,
                    listen_port,
                    tls_port,
                    tls_key,
                    tls_cert,
                    enable_logger,
                    enable_compress,
                    cache,
                )
                .await?;
            } else {
                web::run(
                    db,
                    listen_ip,
                    listen_port,
                    enable_logger,
                    enable_compress,
                    cache,
                )
                .await?;
            }
            Ok(())
        })
}

fn pull(database: String, database_pool_size: usize) -> Result<(), Box<dyn std::error::Error>> {
    tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()?
        .block_on(async {
            let db: DbAbstraction = match DbAbstraction::new(database, database_pool_size).await {
                Ok(db) => db,
                Err(err) => {
                    panic!("DB connection could not be opened {}", err);
                }
            };
            pull::pull(db).await?;
            Ok(())
        })
}

fn main() -> std::io::Result<()> {
    env_logger::init();
    let args = Cli::parse();

    match args.command {
        Commands::Run {
            tls_key,
            tls_cert,
            tls,
            listen_ip,
            listen_port,
            tls_port,
            enable_logger,
            enable_compress,
        } => {
            run(
                args.database,
                args.database_pool_size,
                tls_key,
                tls_cert,
                tls,
                listen_ip,
                listen_port,
                tls_port,
                enable_logger,
                enable_compress,
                args.cacheserver,
                args.cacheserver_ttl,
            )?;
            Ok(())
        }
        Commands::Migrate {} => {
            if DbAbstraction::migrate(args.database).is_err() {
                error!("Error on migrate");
            }
            Ok(())
        }
        Commands::Pull {} => {
            match pull(args.database, args.database_pool_size) {
                Ok(_) => {
                    info!("pull ok");
                }
                Err(err) => {
                    error!("error on pull {}", err);
                }
            }
            Ok(())
        }
    }
}
