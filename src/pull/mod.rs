use crate::db::{DbAbstraction, StationHistory};
use log::{debug, info, trace};
use radiobrowser::RadioBrowserAPI;
use std::error::Error;

async fn pull_single_server<P: AsRef<str>>(
    db: &mut DbAbstraction,
    srvname: P,
) -> Result<(), Box<dyn Error>> {
    let chunksize = 100;
    trace!(
        "pull_single_server(srvname={}) chunksize={}",
        srvname.as_ref(),
        chunksize
    );
    let mut api = RadioBrowserAPI::new_from_dns_a(&srvname).await?;
    let remoteserver = db.get_remote_pull_server(&srvname).await?;
    let mut lastuuid = remoteserver.map(|srv| srv.last_station_uuid).flatten();

    loop {
        let changes: Vec<StationHistory> = api
            .get_station_changes(chunksize, lastuuid)
            .await?
            .into_iter()
            .map(|item| item.into())
            .collect();
        let lastchange = changes.last().map(|item| item.changeuuid.clone());
        if let Some(lastchange) = lastchange {
            db.insert_changes(changes).await?;
            db.update_remote_pull_server_lastuuid(&srvname, &lastchange)
                .await?;
            lastuuid = Some(lastchange.clone());
        } else {
            break;
        }
    }
    Ok(())
}

pub async fn pull(mut db: DbAbstraction) -> Result<(), Box<dyn Error>> {
    info!("Pull server run..");
    let serverlist_new = RadioBrowserAPI::get_default_servers().await?;
    let serverlist_current = db.get_remote_pull_server_list().await?;
    let mut sync_server_count = 1;

    // try to sync from servers currently in the database
    for server in serverlist_current {
        debug!("Pull server {}", server.name);

        let result = pull_single_server(&mut db, &server.name).await;
        match result {
            Ok(_) => {
                sync_server_count -= 1;
                if sync_server_count <= 0 {
                    break;
                }
            }
            Err(error) => {
                debug!("Error on pull: {}", error);
            }
        }
    }

    // sync for current servers was not enough
    if sync_server_count > 0 {
        for server in serverlist_new {
            debug!("Pull new server {}", server);

            let result = pull_single_server(&mut db, &server).await;
            match result {
                Ok(_) => {
                    sync_server_count -= 1;
                    if sync_server_count <= 0 {
                        break;
                    }
                }
                Err(error) => {
                    debug!("Error on pull: {:?}", error);
                }
            }
        }
    }

    Ok(())
}

impl From<radiobrowser::ApiStationHistory> for StationHistory {
    fn from(value: radiobrowser::ApiStationHistory) -> Self {
        StationHistory {
            stationuuid: value.stationuuid,
            changeuuid: value.changeuuid,
            name: value.name,
            url: value.url,
            homepage: value.homepage,
            favicon: value.favicon,
            countrycode: value.countrycode,
            state: value.state,
            language: value.language,
            tags: value.tags,
            geo_lat: value.geo_lat,
            geo_long: value.geo_long,
            source: String::new(),
        }
    }
}
