use crate::db::{DbAbstraction, DbError, StationClick};
use actix_web::{get, post, web, HttpRequest, Responder};
use log::error;
use serde::Serialize;
use uuid::Uuid;

#[derive(Serialize, Debug, Clone)]
pub struct ApiStationClickResult {
    pub stationuuid: String,
    pub clickuuid: String,
    //pub clicktimestamp_iso8601: String,
    //pub clicktimestamp: String,
}

impl From<StationClick> for ApiStationClickResult {
    fn from(value: StationClick) -> Self {
        ApiStationClickResult {
            stationuuid: value.stationuuid,
            clickuuid: value.clickuuid,
        }
    }
}

#[get("/{format}/url/{stationuuid}")]
pub async fn get_insert_station_click(
    db: web::Data<DbAbstraction>,
    path: web::Path<(String, Uuid)>,
    req: HttpRequest,
) -> Result<impl Responder, DbError> {
    let (_format, stationuuid) = path.into_inner();
    let info = req.connection_info();
    let ip = info.peer_addr();
    match ip {
        Some(ip) => db.insert_station_click(stationuuid, ip).await?,
        None => {
            error!("No peer ip for click station request");
        }
    }

    Ok(web::Json(()))
}

#[post("/{format}/url/{stationuuid}")]
pub async fn post_insert_station_click(
    db: web::Data<DbAbstraction>,
    path: web::Path<(String, Uuid)>,
    req: HttpRequest,
) -> Result<impl Responder, DbError> {
    let (_format, stationuuid) = path.into_inner();
    let info = req.connection_info();
    let ip = info.peer_addr();
    match ip {
        Some(ip) => db.insert_station_click(stationuuid, ip).await?,
        None => {
            error!("No peer ip for click station request");
        }
    }

    Ok(web::Json(()))
}

#[get("/{format}/clicks")]
pub async fn get_list_station_click(
    db: web::Data<DbAbstraction>,
    path: web::Path<(String,)>,
) -> Result<impl Responder, DbError> {
    let (_format,) = path.into_inner();
    let list: Vec<ApiStationClickResult> = db
        .get_station_click(None)
        .await?
        .into_iter()
        .map(|item| item.into())
        .collect();
    Ok(web::Json(list))
}

#[get("/{format}/clicks/{stationuuid}")]
pub async fn get_list_station_click_by_uuid(
    db: web::Data<DbAbstraction>,
    path: web::Path<(String, Uuid)>,
) -> Result<impl Responder, DbError> {
    let (_format, stationuuid) = path.into_inner();
    let list: Vec<ApiStationClickResult> = db
        .get_station_click(Some(stationuuid))
        .await?
        .into_iter()
        .map(|item| item.into())
        .collect();
    Ok(web::Json(list))
}
