mod all_extractors;
pub mod cache;
mod error;
mod station_add;
mod station_click;
mod stations;
mod stats;
mod status;

use crate::db::DbAbstraction;
use actix_web::get;
use actix_web::middleware::Compress;
use actix_web::middleware::Condition;
use actix_web::middleware::Logger;
use actix_web::HttpResponse;
use actix_web::Responder;
use actix_web::{web, App, HttpServer};
use actix_web_prometheus::PrometheusMetricsBuilder;
use log::info;
use rustls::{Certificate, ServerConfig};
use rustls_pemfile::{certs, pkcs8_private_keys};
use serde::Serialize;
use station_add::json_insert_stations;
use station_click::get_insert_station_click;
use station_click::get_list_station_click;
use station_click::get_list_station_click_by_uuid;
use station_click::post_insert_station_click;
use stations::csv_get_stations;
use stations::json_get_stations;
use stats::get_json_status;
use status::get_status;
use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::BufReader;

use self::cache::CacheAbstraction;
use self::error::UserFacingError;

pub fn to_csv<S: Serialize>(list: Vec<S>) -> Result<String, Box<dyn Error>> {
    let mut wtr = csv::Writer::from_writer(Vec::new());

    for entry in list {
        wtr.serialize(entry)?;
    }
    wtr.flush()?;
    let x: Vec<u8> = wtr.into_inner()?;
    let out = String::from_utf8(x)?;
    Ok(out)
}

#[get("/")]
pub async fn index() -> impl Responder {
    HttpResponse::Ok().body("This is the radiobrowser api server 0.8 ALPHA")
}

pub fn init(cfg: &mut web::ServiceConfig) {
    cfg.service(index);
    cfg.service(csv_get_stations);
    cfg.service(json_get_stations);
    cfg.service(json_insert_stations);
    cfg.service(get_json_status);
    cfg.service(get_status);
    cfg.service(get_insert_station_click);
    cfg.service(post_insert_station_click);
    cfg.service(get_list_station_click);
    cfg.service(get_list_station_click_by_uuid);
}

pub async fn run(
    db: DbAbstraction,
    listen_ip: String,
    port: usize,
    enable_logger: bool,
    enable_compress: bool,
    cache: CacheAbstraction,
) -> std::io::Result<()> {
    info!("Binding to port: {}", port);
    let labels = HashMap::new();
    //labels.insert("label1".to_string(), "value1".to_string());
    let prometheus = PrometheusMetricsBuilder::new("radiobrowser")
        .endpoint("/metrics")
        .const_labels(labels)
        .build()
        .expect("Unable to initialize prometheus");

    HttpServer::new(move || {
        App::new()
            .wrap(Condition::new(enable_logger, Logger::default()))
            .wrap(Condition::new(enable_compress, Compress::default()))
            .wrap(prometheus.clone())
            .app_data(web::Data::new(db.clone()))
            .app_data(web::Data::new(cache.clone()))
            .configure(init)
    })
    .bind(format!("{}:{}", listen_ip, port))?
    .run()
    .await
}

pub async fn run_tls(
    db: DbAbstraction,
    listen_ip: String,
    port: usize,
    tls_port: usize,
    tls_key: String,
    tls_cert: String,
    enable_logger: bool,
    enable_compress: bool,
    cache: CacheAbstraction,
) -> std::io::Result<()> {
    // Load key files
    let cert_file = &mut BufReader::new(File::open(tls_cert)?);
    let key_file = &mut BufReader::new(File::open(tls_key)?);

    let mut certs_bin = certs(cert_file)?;
    let keys_bin = pkcs8_private_keys(key_file)?;
    assert_eq!(certs_bin.len() > 0, true);
    assert_eq!(keys_bin.len(), 1);

    let labels = HashMap::new();
    //labels.insert("label1".to_string(), "value1".to_string());
    let prometheus = PrometheusMetricsBuilder::new("radiobrowser")
        .endpoint("/metrics")
        .const_labels(labels)
        .build()
        .expect("Unable to initialize prometheus");

    let cert: Vec<Certificate> = certs_bin.drain(..).map(|item| Certificate(item)).collect();
    let key = rustls::PrivateKey(keys_bin[0].clone());
    let config = ServerConfig::builder()
        .with_safe_defaults()
        .with_no_client_auth()
        .with_single_cert(cert, key)
        .expect("bad certificate/key");
    info!("Binding to ports: {}, {}", port, tls_port);
    HttpServer::new(move || {
        App::new()
            .wrap(Condition::new(enable_logger, Logger::default()))
            .wrap(Condition::new(enable_compress, Compress::default()))
            .wrap(prometheus.clone())
            .app_data(web::Data::new(db.clone()))
            .app_data(web::Data::new(cache.clone()))
            .configure(init)
    })
    .bind(format!("{}:{}", listen_ip, port))?
    .bind_rustls(format!("{}:{}", listen_ip, tls_port), config)?
    .run()
    .await
}

impl From<crate::db::DbError> for actix_web::Error {
    fn from(_value: crate::db::DbError) -> Self {
        actix_web::error::ErrorInternalServerError("Database error")
    }
}

impl From<UserFacingError> for actix_web::Error {
    fn from(value: UserFacingError) -> Self {
        match value {
            UserFacingError::Cache => actix_web::error::ErrorInternalServerError("Cache error"),
            UserFacingError::Db => actix_web::error::ErrorInternalServerError("Database error"),
            UserFacingError::Decode => actix_web::error::ErrorBadRequest("Decode error"),
            UserFacingError::UnsupportedOutputFormat => actix_web::error::ErrorBadRequest("Unsupported format"),
            UserFacingError::UnknownContentType => {
                actix_web::error::ErrorBadRequest("UnknownContentType")
            }
        }
    }
}
