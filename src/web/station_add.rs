use crate::db::{DbAbstraction, NewStation};
use actix_web::{post, web, HttpRequest, Responder};
use log::error;
use serde::Deserialize;

#[derive(Deserialize)]
struct ApiStation {
    name: String,
    url: String,
    homepage: String,
    favicon: String,
    countrycode: String,
    state: String,
    language: String,
    tags: String,
    geo_lat: Option<f64>,
    geo_long: Option<f64>,
}

impl From<ApiStation> for crate::db::NewStation {
    fn from(value: ApiStation) -> Self {
        crate::db::NewStation {
            name: value.name,
            url: value.url,
            homepage: value.homepage,
            favicon: value.favicon,
            countrycode: value.countrycode,
            state: value.state,
            language: value.language,
            tags: value.tags,
            geo_lat: value.geo_lat,
            geo_long: value.geo_long,
        }
    }
}

#[post("/json/add")]
async fn json_insert_stations(
    db: web::Data<DbAbstraction>,
    web::Json(apistation): web::Json<ApiStation>,
    _req: HttpRequest,
) -> impl Responder {
    let newstation: NewStation = apistation.into();
    match db.insert_change(newstation).await {
        Ok(_) => {}
        Err(err) => {
            error!("Error: {:?}", err)
        }
    }
    web::Json(())
}
