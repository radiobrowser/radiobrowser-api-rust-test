use log::error;

use super::cache::CacheError;
use crate::db::DbError;
use std::fmt;

#[derive(Debug, Clone)]
pub enum UserFacingError {
    Cache,
    Db,
    Decode,
    UnknownContentType,
    UnsupportedOutputFormat,
}

impl fmt::Display for UserFacingError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "CacheError")
    }
}

impl std::error::Error for UserFacingError {}

impl From<CacheError> for UserFacingError {
    fn from(err: CacheError) -> Self {
        error!("{:?}", err);
        UserFacingError::Cache
    }
}

impl From<DbError> for UserFacingError {
    fn from(err: DbError) -> Self {
        error!("{:?}", err);
        UserFacingError::Db
    }
}
