use actix_multipart::Multipart;
use actix_web::http::header;
use actix_web::FromRequest;
use actix_web::{web, HttpRequest};
use merge::Merge;
use mysql::serde_json;
use serde::de::DeserializeOwned;
use std::collections::HashMap;
use std::fmt::Debug;

use super::error::UserFacingError;

pub async fn get_params<'a, T>(
    req: HttpRequest,
    payload: web::Payload,
) -> Result<T, UserFacingError>
where
    T: Merge + Debug + DeserializeOwned + Clone + 'static,
{
    let query: web::Query<T> = web::Query::extract(&req)
        .await
        .map_err(|_| UserFacingError::Decode)?;
    let mut query = query.into_inner();
    if req.method() == "POST" {
        let mut p: actix_web::dev::Payload = payload.into_inner();
        let contenttype = req
            .headers()
            .get(header::CONTENT_TYPE)
            .ok_or(UserFacingError::UnknownContentType)?
            .to_str()
            .map_err(|_| UserFacingError::UnknownContentType)?;
        match contenttype {
            "application/x-www-form-urlencoded" => {
                let form_url_encoded: web::Form<T> = web::Form::from_request(&req, &mut p)
                    .await
                    .map_err(|_| UserFacingError::Decode)?;
                let form_url_encoded: T = form_url_encoded.into_inner();
                query.merge(form_url_encoded);
            }
            "application/json" => {
                let json: web::Json<T> = web::Json::from_request(&req, &mut p)
                    .await
                    .map_err(|_| UserFacingError::Decode)?;
                query.merge(json.into_inner());
            }
            _ => {
                let multi: Option<Multipart> = Multipart::from_request(&req, &mut p).await.ok();
                let mut map: HashMap<String, String> = HashMap::new();
                use futures::StreamExt as _;
                if let Some(mut multipartoutput) = multi {
                    while let Some(item) = multipartoutput.next().await {
                        let mut field = item.map_err(|_| UserFacingError::Decode)?;

                        while let Some(chunk) = field.next().await {
                            let chunk = chunk.map_err(|_| UserFacingError::Decode)?;
                            let value =
                                std::str::from_utf8(&chunk).map_err(|_| UserFacingError::Decode)?;
                            map.entry(field.name().to_string())
                                .or_insert(value.to_string());
                        }
                    }
                }
                let j = serde_json::to_string(&map).map_err(|_| UserFacingError::Decode)?;
                let p: T = serde_json::from_str(&j).map_err(|_| UserFacingError::Decode)?;
                query.merge(p);
            }
        }
    }
    Ok(query)
}

/*
use actix_web::{web::Payload, FromRequest, HttpRequest};
use futures::ready;
use serde::{de::DeserializeOwned, Serialize};
use std::{ops, pin::Pin, task::Poll};

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug)]
pub struct MergedExtractors<T>(pub T);

impl<T> MergedExtractors<T> {
    /// Unwrap into inner `T` value.
    pub fn into_inner(self) -> T {
        self.0
    }
}

impl<T> ops::Deref for MergedExtractors<T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.0
    }
}

impl<T> ops::DerefMut for MergedExtractors<T> {
    fn deref_mut(&mut self) -> &mut T {
        &mut self.0
    }
}

impl<T> FromRequest for MergedExtractors<T>
where
    T: DeserializeOwned + 'static,
{
    type Error = Error;
    type Future = FormExtractFut<T>;

    #[inline]
    fn from_request(req: &HttpRequest, payload: &mut Payload) -> Self::Future {
        MergedExtractorsFut {
            fut: UrlEncoded::new(req, payload).limit(limit),
            req: req.clone(),
            err_handler,
        }
    }
}

pub struct MergedExtractorsFut<T> {
    req: Option<HttpRequest>,
    fut: JsonBody<T>,
    err_handler: JsonErrorHandler,
}

impl<T: DeserializeOwned> Future for MergedExtractorsFut<T> {
    type Output = Result<Json<T>, Error>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.get_mut();

        let res = ready!(Pin::new(&mut this.fut).poll(cx));

        let res = match res {
            Err(err) => {
                let req = this.req.take().unwrap();
                log::debug!(
                    "Failed to deserialize Json from payload. \
                         Request path: {}",
                    req.path()
                );

                if let Some(err_handler) = this.err_handler.as_ref() {
                    Err((*err_handler)(err, &req))
                } else {
                    Err(err.into())
                }
            }
            Ok(data) => Ok(Json(data)),
        };

        Poll::Ready(res)
    }
}
*/
