mod error;
mod redis;

pub use error::CacheError;
use log::trace;
use redis::RedisCache;
pub use redis::RedisError;
use serde::{de::DeserializeOwned, Serialize};

#[derive(Clone)]
pub enum CacheAbstraction {
    None,
    Redis(RedisCache),
}

impl CacheAbstraction {
    pub fn new<T: AsRef<str>>(url: Option<T>, ttl: u32) -> Result<Self, CacheError> {
        match url {
            Some(url) => {
                if url.as_ref().starts_with("postgres:") {
                    Ok(CacheAbstraction::None)
                } else if url.as_ref().starts_with("redis:") {
                    Ok(CacheAbstraction::Redis(RedisCache::new(url.as_ref(), ttl)?))
                } else {
                    Err(CacheError::UnknownUrl)
                }
            }
            None => Ok(CacheAbstraction::None),
        }
    }

    pub async fn cache_save<T, S>(&self, key: T, value: S) -> Result<(), CacheError>
    where
        T: Serialize,
        S: Serialize,
    {
        trace!("cache_save()");
        match self {
            CacheAbstraction::None => Ok(()),
            CacheAbstraction::Redis(obj) => Ok(obj.cache_save(key, value).await?),
        }
    }

    pub async fn cache_load<'a, T, U>(&self, key: T) -> Result<Option<U>, CacheError>
    where
        T: Serialize,
        U: DeserializeOwned + Default,
    {
        trace!("cache_load()");
        match self {
            CacheAbstraction::None => Ok(None),
            CacheAbstraction::Redis(obj) => Ok(obj.cache_load(key).await?),
        }
    }
}
