use std::fmt;

use super::RedisError;

#[derive(Debug)]
pub enum CacheError {
    Redis(RedisError),
    UnknownUrl,
}

impl fmt::Display for CacheError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "CacheError")
    }
}

impl std::error::Error for CacheError {}

impl From<RedisError> for CacheError {
    fn from(err: RedisError) -> Self {
        CacheError::Redis(err)
    }
}
