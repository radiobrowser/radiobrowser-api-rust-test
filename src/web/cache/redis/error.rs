use std::fmt;

use deadpool_redis::PoolError;

#[derive(Debug)]
pub enum RedisError {
    Generic(String),
    Decode(serde_json::error::Error),
    Pool(PoolError),
    Redis(deadpool_redis::redis::RedisError),
}

impl fmt::Display for RedisError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "RedisError")
    }
}

impl std::error::Error for RedisError {}

impl From<serde_json::error::Error> for RedisError {
    fn from(value: serde_json::error::Error) -> Self {
        RedisError::Decode(value)
    }
}

impl From<PoolError> for RedisError {
    fn from(value: PoolError) -> Self {
        RedisError::Pool(value)
    }
}

impl From<deadpool_redis::redis::RedisError> for RedisError {
    fn from(value: deadpool_redis::redis::RedisError) -> Self {
        RedisError::Redis(value)
    }
}
