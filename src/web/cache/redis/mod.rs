mod error;

use deadpool_redis::{redis::cmd, Pool};
pub use error::RedisError;
use log::trace;
use serde::{de::DeserializeOwned, Serialize};

#[derive(Clone)]
pub struct RedisCache {
    pub pool: Pool,
    pub ttl: u32,
}

impl RedisCache {
    pub fn new<T: AsRef<str>>(url: T, ttl: u32) -> Result<Self, RedisError> {
        let cfg = deadpool_redis::Config::from_url(url.as_ref());
        match cfg.create_pool(Some(deadpool_postgres::Runtime::Tokio1)) {
            Ok(pool) => Ok(RedisCache { pool, ttl }),
            Err(err) => Err(RedisError::Generic(format!(
                "Cache connection could not be opened {}",
                err
            ))),
        }
    }

    pub async fn cache_save<T, S>(&self, key: T, value: S) -> Result<(), RedisError>
    where
        T: Serialize,
        S: Serialize,
    {
        let key = serde_json::to_string(&key)?;
        let value = serde_json::to_string(&value)?;
        let mut conn = self.pool.get().await?;
        trace!("set value: {} {}", key, value);
        cmd("SET")
            .arg(&[&key, &value, "EX", &self.ttl.to_string()])
            .query_async(&mut conn)
            .await?;
        Ok(())
    }

    pub async fn cache_load<'a, T, U>(&self, key: T) -> Result<Option<U>, RedisError>
    where
        T: Serialize,
        U: DeserializeOwned + Default,
    {
        let key = serde_json::to_string(&key)?;
        trace!("get value: {}", key);
        let mut conn = self.pool.get().await?;
        let value: Option<String> = cmd("GET").arg(&[&key]).query_async(&mut conn).await?;
        trace!("value: {:?}", value);
        if let Some(value) = value {
            let value2: U = serde_json::from_str(&value)?;
            Ok(Some(value2))
        } else {
            Ok(None)
        }
    }
}
