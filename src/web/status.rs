use crate::db::DbAbstraction;
use actix_web::{get, web, HttpRequest};
use log::info;
use serde::Serialize;

#[derive(Serialize)]
pub struct StatusResponse {
    status: String,
}

#[get("/status")]
pub async fn get_status(
    db: web::Data<DbAbstraction>,
    _req: HttpRequest,
) -> actix_web::Result<String> {
    info!("/status");
    db.ping().await.map_err(actix_web::error::ErrorGone)?;
    Ok("OK".to_string())
}
