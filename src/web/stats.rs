use crate::db::DbAbstraction;
use actix_web::{get, web, HttpRequest};
use clap::crate_version;
use log::info;
use serde::Serialize;

#[derive(Serialize)]
pub struct StatusResponse {
    pub supported_version: u32,
    pub software_version: String,
    pub status: String,
    pub stations: u64,
    pub stations_broken: u64,
    pub tags: u64,
    pub clicks_last_hour: u64,
    pub clicks_last_day: u64,
    pub languages: u64,
    pub countries: u64,
}

#[get("/json/stats")]
pub async fn get_json_status(
    db: web::Data<DbAbstraction>,
    _req: HttpRequest,
) -> actix_web::Result<web::Json<StatusResponse>> {
    info!("/json/stats");
    db.ping().await.map_err(actix_web::error::ErrorGone)?;
    Ok(web::Json(StatusResponse {
        supported_version: 1,
        software_version: crate_version!().to_string(),
        status: "OK".to_string(),
        stations: db.get_station_count().await?,
        stations_broken: 0,
        tags: 0,
        clicks_last_hour: 0,
        clicks_last_day: 0,
        languages: 0,
        countries: 0,
    }))
}
