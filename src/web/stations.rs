use super::all_extractors::get_params;
use super::to_csv;
use crate::db::DbAbstraction;
use crate::web::cache::CacheAbstraction;
use crate::web::error::UserFacingError;
use actix_web::http::header::ContentType;
use actix_web::{get, web, HttpRequest, HttpResponse, Responder};
use log::info;
use merge::Merge;
use serde::{Deserialize, Serialize};
use serde_aux::prelude::*;
use std::fmt::Debug;

#[derive(Serialize, Deserialize, Debug, Merge, Clone)]
pub struct TestForm {
    pub order: Option<String>,
    pub reverse: Option<bool>,
    #[serde(default, deserialize_with = "deserialize_option_number_from_string")]
    pub offset: Option<u64>,
    #[serde(default, deserialize_with = "deserialize_option_number_from_string")]
    pub limit: Option<u64>,
    pub hidebroken: Option<bool>,
}

impl TestForm {
    pub fn offset(&self) -> u64 {
        self.offset.unwrap_or(0)
    }
    pub fn limit(&self) -> u64 {
        self.limit.unwrap_or(100)
    }
}

/*
fn stationlist_to_responder(
    format: &str,
    list: Vec<Station>,
) -> Result<HttpResponse,UserFacingError> {
    HttpResponse::
    match format {
        "json" => Ok(web::Json(list)),
        "csv" => {
            let out = to_csv(list).map_err(|_err| UserFacingError::Decode)?;
            Ok((HttpResponse::Ok()
                .content_type(ContentType(mime::TEXT_CSV_UTF_8))
                .body(out))
        }
        _ => {
            Err(UserFacingError::UnsupportedOutputFormat)
        }
    }
}
 */

/*
async fn get_cached<T, V>(
    db: web::Data<DbAbstraction>,
    cache: web::Data<CacheAbstraction>,
    query: V,
) -> Result<T, UserFacingError>
where
    V: Serialize,
{
    Ok(match cache.cache_load(&query).await? {
        Some(list) => list,
        None => {
            let list = db.get_stations(query.offset(), query.limit()).await?;
            cache.cache_save(&query, &list).await?;
            list
        }
    })
}
*/

#[get("/csv/stations")]
pub async fn csv_get_stations(
    db: web::Data<DbAbstraction>,
    cache: web::Data<CacheAbstraction>,
    req: HttpRequest,
    payload: web::Payload,
) -> Result<impl Responder, UserFacingError> {
    info!("/format/stations");
    let query: TestForm = get_params(req, payload).await?;
    let list = match cache.cache_load(&query).await? {
        Some(list) => list,
        None => {
            let list = db.get_stations(query.offset(), query.limit()).await?;
            cache.cache_save(&query, &list).await?;
            list
        }
    };

    let out = to_csv(list).map_err(|_err| UserFacingError::Decode)?;
    Ok(HttpResponse::Ok()
        .content_type(ContentType(mime::TEXT_CSV_UTF_8))
        .body(out))
}

#[get("/json/stations")]
pub async fn json_get_stations(
    db: web::Data<DbAbstraction>,
    cache: web::Data<CacheAbstraction>,
    req: HttpRequest,
    payload: web::Payload,
) -> Result<impl Responder, UserFacingError> {
    info!("/json/stations");
    let query: TestForm = get_params(req, payload).await?;
    let list = match cache.cache_load(&query).await? {
        Some(list) => list,
        None => {
            let list = db.get_stations(query.offset(), query.limit()).await?;
            cache.cache_save(&query, &list).await?;
            list
        }
    };
    Ok(web::Json(list))
}
