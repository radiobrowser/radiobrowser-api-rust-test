mod db_error;
mod mongo;
mod my;
mod pg;
mod remote_server;
mod station;
mod stationhistory;
mod station_click;

pub use self::db_error::DbError;
use log::debug;
use log::trace;
pub use remote_server::RemoteServer;
pub use station::NewStation;
pub use station::Station;
pub use stationhistory::StationHistory;
use uuid::Uuid;
use std::collections::HashMap;
pub use station_click::StationClick;

#[derive(Clone)]
pub struct PG {
    pool: deadpool_postgres::Pool,
}

#[derive(Clone)]
pub struct My {
    pool: mysql_async::Pool,
}

#[derive(Clone)]
pub struct Mongo {
    client: mongodb::Client,
}

#[derive(Clone)]
pub enum DbAbstraction {
    PG(PG),
    My(My),
    Mongo(Mongo),
}

impl DbAbstraction {
    pub async fn new<P: AsRef<str>>(url: P, poolsize: usize) -> Result<Self, DbError> {
        trace!("new()");
        if url.as_ref().starts_with("postgres:") {
            Ok(DbAbstraction::PG(PG::new(
                url.as_ref().to_string(),
                poolsize,
            )?))
        } else if url.as_ref().starts_with("mysql:") {
            Ok(DbAbstraction::My(My::new(url.as_ref().to_string())?))
        } else if url.as_ref().starts_with("mongodb:") || url.as_ref().starts_with("mongodb+srv:") {
            Ok(DbAbstraction::Mongo(
                Mongo::new(url.as_ref().to_string()).await?,
            ))
        } else {
            Err(DbError::UnknownUrl)
        }
    }

    pub fn migrate<P: AsRef<str>>(url: P) -> Result<(), DbError> {
        trace!("migrate()");
        if url.as_ref().starts_with("postgres:") {
            Ok(PG::migrate(url.as_ref().to_string())?)
        } else if url.as_ref().starts_with("mysql:") {
            Ok(My::migrate(url.as_ref().to_string())?)
        } else if url.as_ref().starts_with("mongodb:") || url.as_ref().starts_with("mongodb+srv:") {
            Ok(Mongo::migrate(url.as_ref().to_string())?)
        } else {
            Err(DbError::UnknownUrl)
        }
    }

    async fn insert_stations_raw(&self, stations: &[Station]) -> Result<(), DbError> {
        trace!("insert_stations_raw() count={}", stations.len());
        match self {
            DbAbstraction::PG(obj) => Ok(obj.insert_stations_raw(stations).await?),
            DbAbstraction::My(obj) => Ok(obj.insert_stations_raw(stations).await?),
            DbAbstraction::Mongo(obj) => Ok(obj.insert_stations_raw(stations).await?),
        }
    }

    async fn update_stations_raw(&self, stations: &[Station]) -> Result<(), DbError> {
        trace!("update_stations_raw() count={}", stations.len());
        match self {
            DbAbstraction::PG(obj) => Ok(obj.update_stations_raw(stations).await?),
            DbAbstraction::My(obj) => Ok(obj.update_stations_raw(stations).await?),
            DbAbstraction::Mongo(obj) => Ok(obj.update_stations_raw(stations).await?),
        }
    }

    async fn insert_changes_raw(&self, changes: &[StationHistory]) -> Result<(), DbError> {
        trace!("insert_changes_raw()");
        match self {
            DbAbstraction::PG(obj) => obj.insert_changes_raw(changes).await?,
            DbAbstraction::My(obj) => obj.insert_changes_raw(changes).await?,
            DbAbstraction::Mongo(obj) => obj.insert_changes_raw(changes).await?,
        };
        Ok(())
    }

    pub async fn get_changes_by_uuid(
        &self,
        changeuuids: &[String],
    ) -> Result<Vec<StationHistory>, DbError> {
        trace!("get_changes_by_uuid()");
        match self {
            DbAbstraction::PG(obj) => Ok(obj.get_changes_by_uuid(changeuuids).await?),
            DbAbstraction::My(obj) => Ok(obj.get_changes_by_uuid(changeuuids).await?),
            DbAbstraction::Mongo(obj) => Ok(obj.get_changes_by_uuid(changeuuids).await?),
        }
    }

    pub async fn get_stations_by_uuid(
        &self,
        stationuuids: &[String],
    ) -> Result<Vec<Station>, DbError> {
        trace!("get_stations_by_uuid()");
        match self {
            DbAbstraction::PG(obj) => Ok(obj.get_stations_by_uuid(stationuuids).await?),
            DbAbstraction::My(obj) => Ok(obj.get_stations_by_uuid(stationuuids).await?),
            DbAbstraction::Mongo(obj) => Ok(obj.get_stations_by_uuid(stationuuids).await?),
        }
    }

    pub async fn get_stations(&self, offset: u64, limit: u64) -> Result<Vec<Station>, DbError> {
        trace!("get_stations()");
        match self {
            DbAbstraction::PG(obj) => Ok(obj.get_stations(offset, limit).await?),
            DbAbstraction::My(obj) => Ok(obj.get_stations(offset, limit).await?),
            DbAbstraction::Mongo(obj) => Ok(obj.get_stations(offset, limit).await?),
        }
    }

    pub async fn ping(&self) -> Result<(), DbError> {
        trace!("ping()");
        match self {
            DbAbstraction::PG(obj) => obj.ping().await?,
            DbAbstraction::My(obj) => obj.ping().await?,
            DbAbstraction::Mongo(obj) => obj.ping().await?,
        }
        Ok(())
    }

    pub async fn get_station_count(&self) -> Result<u64, DbError> {
        trace!("get_station_count()");
        match self {
            DbAbstraction::PG(obj) => Ok(obj.get_station_count().await? as u64),
            DbAbstraction::My(obj) => Ok(obj.get_station_count().await?),
            DbAbstraction::Mongo(obj) => Ok(obj.get_station_count().await?),
        }
    }

    pub async fn get_remote_pull_server_list(&self) -> Result<Vec<RemoteServer>, DbError> {
        trace!("get_remote_pull_server_list()");
        match self {
            DbAbstraction::PG(obj) => Ok(obj.get_remote_pull_server_list().await?),
            DbAbstraction::My(obj) => Ok(obj.get_remote_pull_server_list().await?),
            DbAbstraction::Mongo(obj) => Ok(obj.get_remote_pull_server_list().await?),
        }
    }

    pub async fn update_remote_pull_server_lastuuid<P: AsRef<str>, Q: AsRef<str>>(
        &self,
        servername: P,
        lastuuid: Q,
    ) -> Result<(), DbError> {
        trace!("update_remote_pull_server_lastuuid()");
        match self {
            DbAbstraction::PG(obj) => Ok(obj
                .update_remote_pull_server_lastuuid(servername, lastuuid)
                .await?),
            DbAbstraction::My(obj) => Ok(obj
                .update_remote_pull_server_lastuuid(servername, lastuuid)
                .await?),
            DbAbstraction::Mongo(obj) => Ok(obj
                .update_remote_pull_server_lastuuid(servername, lastuuid)
                .await?),
        }
    }

    pub async fn get_remote_pull_server<P: AsRef<str>>(
        &self,
        servername: P,
    ) -> Result<Option<RemoteServer>, DbError> {
        trace!("get_remote_pull_server()");
        match self {
            DbAbstraction::PG(obj) => Ok(obj.get_remote_pull_server(servername).await?),
            DbAbstraction::My(obj) => Ok(obj.get_remote_pull_server(servername).await?),
            DbAbstraction::Mongo(obj) => Ok(obj.get_remote_pull_server(servername).await?),
        }
    }

    async fn insert_stations_internal(&self, stations: Vec<Station>) -> Result<(), DbError> {
        trace!("insert_stations() count={}", stations.len());
        let stations_len = stations.len();

        let stationuuids: Vec<String> = stations
            .iter()
            .map(|item| item.stationuuid.to_string())
            .collect();
        let result = self.get_stations_by_uuid(&stationuuids).await?;
        let found_stationuuids: Vec<String> = result
            .iter()
            .map(|item| item.stationuuid.to_string())
            .collect();

        let mut to_insert: HashMap<String, Station> = HashMap::new();
        let mut to_update: HashMap<String, Station> = HashMap::new();
        // use only the last station of a given stationuuid
        for station in stations.into_iter() {
            if found_stationuuids.contains(&station.stationuuid) {
                // remove if existing
                to_update.remove(&station.stationuuid);
                // add new entry
                to_update.insert(station.stationuuid.to_string(), station);
            } else {
                // remove if existing
                to_insert.remove(&station.stationuuid);
                // add new entry
                to_insert.insert(station.stationuuid.to_string(), station);
            }
        }

        debug!(
            "Inserting {}, updating {}, ignore {}",
            to_insert.len(),
            to_update.len(),
            stations_len - to_update.len() - to_insert.len(),
        );

        if to_insert.len() > 0 {
            let list: Vec<Station> = to_insert.into_values().collect();
            self.insert_stations_raw(&list).await?;
        }

        if to_update.len() > 0 {
            let list: Vec<Station> = to_update.into_values().collect();
            self.update_stations_raw(&list).await?;
        }

        Ok(())
    }

    pub async fn insert_change<T>(&self, change: T) -> Result<(), DbError>
    where
        T: Into<StationHistory>,
    {
        self.insert_changes(vec![change.into()]).await
    }

    pub async fn insert_changes(&self, changes: Vec<StationHistory>) -> Result<(), DbError> {
        trace!("insert_changes() count={}", changes.len());
        let changes_len = changes.len();

        let changeuuids: Vec<String> = changes
            .iter()
            .map(|item| item.changeuuid.to_string())
            .collect();
        let result = self.get_changes_by_uuid(&changeuuids).await?;
        let found_uuids: Vec<String> = result
            .iter()
            .map(|item| item.changeuuid.to_string())
            .collect();

        let filtered_items: Vec<StationHistory> = changes
            .into_iter()
            .filter(|item| !found_uuids.contains(&item.changeuuid))
            .collect();

        debug!(
            "Inserting {}, ignore {}",
            filtered_items.len(),
            changes_len - filtered_items.len(),
        );

        if filtered_items.len() > 0 {
            self.insert_changes_raw(&filtered_items[..]).await?;
            self.insert_stations_internal(
                filtered_items
                    .into_iter()
                    .map(|change| change.into())
                    .collect(),
            )
            .await?;
        }

        Ok(())
    }

    pub async fn insert_station_click<P: AsRef<str>>(&self, stationuuid: Uuid, ip: P) -> Result<(), DbError> {
        trace!("insert_station_click({}, {})", stationuuid, ip.as_ref());
        match self {
            DbAbstraction::PG(obj) => Ok(obj.insert_station_click(stationuuid, ip).await?),
            DbAbstraction::My(obj) => Ok(obj.insert_station_click(stationuuid, ip).await?),
            DbAbstraction::Mongo(obj) => Ok(obj.insert_station_click(stationuuid, ip).await?),
        }
    }

    pub async fn get_station_click(&self, stationuuid: Option<Uuid>) -> Result<Vec<StationClick>, DbError> {
        trace!("get_station_click({:?})", stationuuid);
        match self {
            DbAbstraction::PG(obj) => Ok(obj.get_station_click(stationuuid).await?),
            DbAbstraction::My(obj) => Ok(obj.get_station_click(stationuuid).await?),
            DbAbstraction::Mongo(obj) => Ok(obj.get_station_click(stationuuid).await?),
        }
    }
}
