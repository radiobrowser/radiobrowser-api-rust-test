use serde::{Deserialize, Serialize};
use uuid::Uuid;

use super::NewStation;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct StationHistory {
    pub stationuuid: String,
    pub changeuuid: String,
    pub name: String,
    pub url: String,
    pub homepage: String,
    pub favicon: String,
    pub countrycode: String,
    pub state: String,
    pub language: String,
    pub tags: String,
    pub geo_lat: Option<f64>,
    pub geo_long: Option<f64>,
    pub source: String,
}

impl From<NewStation> for StationHistory {
    fn from(value: NewStation) -> Self {
        StationHistory {
            stationuuid: Uuid::new_v4().to_string(),
            changeuuid: Uuid::new_v4().to_string(),
            name: value.name,
            url: value.url,
            homepage: value.homepage,
            favicon: value.favicon,
            countrycode: value.countrycode,
            state: value.state,
            language: value.language,
            tags: value.tags,
            geo_lat: value.geo_lat,
            geo_long: value.geo_long,
            source: String::new(),
        }
    }
}
