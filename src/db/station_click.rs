use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct StationClick {
    pub stationuuid: String,
    pub clickuuid: String,
    //pub clicktime: String,
    pub ip: String,
}
