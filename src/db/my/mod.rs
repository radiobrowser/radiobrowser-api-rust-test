// Docs: https://docs.rs/mysql_async/latest/mysql_async/index.html

mod error;

use super::{My, RemoteServer, StationClick};
use super::{Station, StationHistory};
pub use error::MyError;
use log::{info, trace};
use mysql_async::{from_row_opt, Row, Value};
use mysql_async::prelude::*;
use std::borrow::Borrow;
use uuid::Uuid;

const TABLE_STATION_NAME: &'static str = "station";
const TABLE_STATION_COLUMNS: &'static str = "stationuuid,changeuuid,name,url,homepage,favicon,countrycode,state,language,tags,geo_lat,geo_long";

const TABLE_STATIONHISTORY_NAME: &'static str = "stationhistory";
const TABLE_STATIONHISTORY_COLUMNS: &'static str = "stationuuid,changeuuid,name,url,homepage,favicon,countrycode,state,language,tags,geo_lat,geo_long";

impl My {
    pub fn new<P: AsRef<str>>(url: P) -> Result<Self, MyError> {
        let pool = mysql_async::Pool::new(url.as_ref());

        Ok(My { pool })
    }

    pub fn migrate<U>(url: U) -> Result<(), MyError>
    where
        U: AsRef<str>,
    {
        info!("Running DB migrations...");
        let pool = mysql::Pool::new(url.as_ref())?;
        let mut conn = pool.get_conn()?;
        mod embedded {
            use refinery::embed_migrations;
            embed_migrations!("./migrations/my");
        }
        let migration_report = embedded::migrations::runner().run(&mut conn)?;
        for migration in migration_report.applied_migrations() {
            info!(
                "Migration Applied -  Name: {}, Version: {}",
                migration.name(),
                migration.version()
            );
        }

        info!("DB migrations finished!");

        Ok(())
    }

    fn station_row_to_struct(&self, row: Row) -> Result<Station, MyError> {
        let (
            stationuuid,
            changeuuid,
            name,
            url,
            homepage,
            favicon,
            countrycode,
            state,
            language,
            tags,
            geo_lat,
            geo_long,
        ) = from_row_opt(row).map_err(|_| MyError::FromRow)?;
        let station = Station {
            stationuuid,
            changeuuid,
            name,
            url,
            homepage,
            favicon,
            countrycode,
            state,
            language,
            tags,
            geo_lat,
            geo_long,
        };
        Ok(station)
    }

    fn stationhistory_row_to_struct(&self, row: Row) -> Result<StationHistory, MyError> {
        let (
            stationuuid,
            changeuuid,
            name,
            url,
            homepage,
            favicon,
            countrycode,
            state,
            language,
            tags,
            geo_lat,
            geo_long,
        ) = from_row_opt(row).map_err(|_| MyError::FromRow)?;
        let station = StationHistory {
            stationuuid,
            changeuuid,
            name,
            url,
            homepage,
            favicon,
            countrycode,
            state,
            language,
            tags,
            geo_lat,
            geo_long,
            source: String::new(),
        };
        Ok(station)
    }

    pub async fn get_stations(&self, offset: u64, limit: u64) -> Result<Vec<Station>, MyError> {
        let mut client = self.pool.get_conn().await?;
        let query = format!(
            "SELECT stationuuid,changeuuid,name,url,homepage,favicon,countrycode,
            state,language,tags,geo_lat,geo_long FROM station LIMIT {},{}",
            offset, limit
        );
        let rows: Vec<Row> = client.query(query).await?;
        let mut stations: Vec<Station> = vec![];
        for row in rows.into_iter() {
            stations.push(self.station_row_to_struct(row)?);
        }
        Ok(stations)
    }

    pub async fn get_changes_by_uuid(
        &self,
        changeuuids: &[String],
    ) -> Result<Vec<StationHistory>, MyError> {
        trace!("get_changes_by_uuid() count={}", changeuuids.len());
        let mut client = self.pool.get_conn().await?;
        let mut list = vec![];
        for chunk in changeuuids.chunks(100) {
            let in_items = (1..=chunk.len())
                .map(|_| format!("?"))
                .collect::<Vec<String>>()
                .join(",");
            let query = format!(
                "SELECT {} FROM {} WHERE changeuuid IN ({})",
                TABLE_STATIONHISTORY_COLUMNS, TABLE_STATIONHISTORY_NAME, in_items
            );
            let p: Vec<Value> = chunk.into_iter().map(|item| item.into()).collect();
            let rows: Vec<Row> = client.exec(query, p).await?;
            for row in rows {
                list.push(self.stationhistory_row_to_struct(row)?);
            }
        }
        Ok(list)
    }

    pub async fn get_stations_by_uuid(
        &self,
        stationuuids: &[String],
    ) -> Result<Vec<Station>, MyError> {
        trace!("get_stations_by_uuid() count={}", stationuuids.len());
        let mut client = self.pool.get_conn().await?;
        let mut list = vec![];
        for chunk in stationuuids.chunks(100) {
            let in_items = (1..=chunk.len())
                .map(|_| format!("?"))
                .collect::<Vec<String>>()
                .join(",");
            let query = format!(
                "SELECT {} FROM {} WHERE stationuuid IN ({})",
                TABLE_STATION_COLUMNS, TABLE_STATION_NAME, in_items
            );
            let p: Vec<Value> = chunk.into_iter().map(|item| item.into()).collect();
            let rows: Vec<Row> = client.exec(query, p).await?;
            for row in rows {
                list.push(self.station_row_to_struct(row)?);
            }
        }
        Ok(list)
    }

    pub async fn insert_changes_raw(&self, changes: &[StationHistory]) -> Result<(), MyError> {
        trace!("insert_changes() count={}", changes.len());
        let mut client = self.pool.get_conn().await?;
        client
            .exec_batch(
                format!(
                    "INSERT INTO {}({}) VALUES(
                            :stationuuid,
                            :changeuuid,
                            :name,
                            :url,
                            :homepage,
                            :favicon,
                            :countrycode,
                            :state,
                            :language,
                            :tags,
                            :geo_lat,
                            :geo_long
                        )",
                    TABLE_STATIONHISTORY_NAME, TABLE_STATIONHISTORY_COLUMNS
                ),
                changes.iter().map(|station| {
                    params! {
                        "stationuuid" => &station.stationuuid,
                        "changeuuid" => &station.changeuuid,
                        "name" => &station.name,
                        "url" => &station.url,
                        "homepage" => &station.homepage,
                        "favicon" => &station.favicon,
                        "countrycode" => &station.countrycode,
                        "state" => &station.state,
                        "language" => &station.language,
                        "tags" => &station.tags,
                        "geo_lat" => &station.geo_lat,
                        "geo_long" => &station.geo_long,
                    }
                }),
            )
            .await?;

        Ok(())
    }

    pub async fn insert_stations_raw(&self, stations: &[Station]) -> Result<(), MyError> {
        let mut client = self.pool.get_conn().await?;
        client
            .exec_batch(
                r"INSERT INTO station(stationuuid,changeuuid,name,url,homepage,favicon,countrycode,state,language,tags,geo_lat,geo_long)VALUES(:stationuuid,:changeuuid,:name,:url,:homepage,:favicon,:countrycode,:state,:language,:tags,:geo_lat,:geo_long)",
                stations.into_iter().map(|station| {
                    let station = station.borrow();
                    params! {
                        "stationuuid" => &station.stationuuid,
                        "changeuuid" => &station.changeuuid,
                        "name" => &station.name,
                        "url" => &station.url,
                        "homepage" => &station.homepage,
                        "favicon" => &station.favicon,
                        "countrycode" => &station.countrycode,
                        "state" => &station.state,
                        "language" => &station.language,
                        "tags" => &station.tags,
                        "geo_lat" => &station.geo_lat,
                        "geo_long" => &station.geo_long,
                    }
                }),
            )
            .await?;

        Ok(())
    }

    pub async fn update_stations_raw(&self, stations: &[Station]) -> Result<(), MyError> {
        let mut client = self.pool.get_conn().await?;
        client
            .exec_batch(
                r"UPDATE station SET changeuuid=:changeuuid,name=:name,
                    url=:url,homepage=:homepage,favicon=:favicon,countrycode=:countrycode,state=:state,
                    language=:language,tags=:tags,geo_lat=:geo_last,geo_long=:geo_long
                    WHERE stationuuid=:stationuuid",
                stations.into_iter().map(|station| {
                    let station = station.borrow();
                    params! {
                        "stationuuid" => &station.stationuuid,
                        "changeuuid" => &station.changeuuid,
                        "name" => &station.name,
                        "url" => &station.url,
                        "homepage" => &station.homepage,
                        "favicon" => &station.favicon,
                        "countrycode" => &station.countrycode,
                        "state" => &station.state,
                        "language" => &station.language,
                        "tags" => &station.tags,
                        "geo_lat" => &station.geo_lat,
                        "geo_long" => &station.geo_long,
                    }
                }),
            )
            .await?;

        Ok(())
    }

    pub async fn ping(&self) -> Result<(), MyError> {
        let mut client = self.pool.get_conn().await?;
        client.exec_drop(r"SELECT 1", ()).await?;
        Ok(())
    }

    pub async fn get_remote_pull_server_list(&self) -> Result<Vec<RemoteServer>, MyError> {
        let mut client = self.pool.get_conn().await?;
        let list = format!("SELECT name,last_station_uuid FROM remoteserver",)
            .with(())
            .map(&mut client, |(name, last_station_uuid)| RemoteServer {
                name,
                last_station_uuid,
            })
            .await?;
        Ok(list)
    }

    pub async fn update_remote_pull_server_lastuuid<P: AsRef<str>, Q: AsRef<str>>(
        &self,
        servername: P,
        lastuuid: Q,
    ) -> Result<(), MyError> {
        trace!(
            "update_remote_pull_server_lastuuid({},{})",
            servername.as_ref(),
            lastuuid.as_ref()
        );
        let mut client = self.pool.get_conn().await?;
        let result = client
            .exec_iter(
                r"UPDATE remoteserver SET last_station_uuid=:last_station_uuid WHERE name=:name",
                params! {
                    "name" => servername.as_ref(),
                    "last_station_uuid" => lastuuid.as_ref(),
                },
            )
            .await?;
        if result.affected_rows() == 0 {
            client
            .exec_drop(
                r"INSERT INTO remoteserver(name,last_station_uuid) VALUES(:name,:last_station_uuid)",
                params! {
                    "name" => servername.as_ref(),
                    "last_station_uuid" => lastuuid.as_ref(),
                },
            )
            .await?;
        }
        Ok(())
    }

    pub async fn get_remote_pull_server<P: AsRef<str>>(
        &self,
        servername: P,
    ) -> Result<Option<RemoteServer>, MyError> {
        let mut client = self.pool.get_conn().await?;
        let mut list = format!("SELECT name,last_station_uuid FROM remoteserver WHERE name=:name",)
            .with(params! {"name"=>servername.as_ref()})
            .map(&mut client, |(name, last_station_uuid)| RemoteServer {
                name,
                last_station_uuid,
            })
            .await?;
        Ok(list.pop())
    }

    pub async fn get_station_count(&self) -> Result<u64, MyError> {
        let mut client = self.pool.get_conn().await?;
        let (count,) = client
            .query_first(r"SELECT COUNT(*) FROM station")
            .await?
            .unwrap_or((0,));
        Ok(count)
    }

    pub async fn insert_station_click<P: AsRef<str>>(
        &self,
        stationuuid: Uuid,
        ip: P,
    ) -> Result<(), MyError> {
        trace!("insert_station_click({}, {})", stationuuid, ip.as_ref());

        let mut client = self.pool.get_conn().await?;
        let query = "INSERT INTO stationclick(stationuuid, clickuuid, ip, clicktime) VALUES(:stationuuid, :clickuuid, :ip, UTC_TIMESTAMP())";
        client
            .exec_drop(
                query,
                params! {
                    "stationuuid" => stationuuid.to_string(),
                    "clickuuid" => Uuid::new_v4().to_string(),
                    "ip" => ip.as_ref(),
                },
            )
            .await?;
        Ok(())
    }

    pub async fn get_station_click(
        &self,
        stationuuid: Option<Uuid>,
    ) -> Result<Vec<StationClick>, MyError> {
        trace!("get_station_click({:?})", stationuuid);
        let mut client = self.pool.get_conn().await?;

        let list = match stationuuid {
            Some(stationuuid) => {
                client
                .exec_map(
                    "SELECT stationuuid,clickuuid,ip FROM stationclick WHERE stationuuid=:stationuuid ORDER BY clicktime",
                    params! {
                        "stationuuid" => stationuuid
                    },
                    |(stationuuid, clickuuid, ip)| StationClick {
                        stationuuid,
                        clickuuid,
                        ip,
                    },
                )
                .await?
            }
            None => {
                client
                .query_map(
                    "SELECT stationuuid,clickuuid,ip FROM stationclick ORDER BY clicktime",
                    |(stationuuid, clickuuid, ip)| StationClick {
                        stationuuid,
                        clickuuid,
                        ip,
                    },
                )
                .await?
            }
        };
        Ok(list)
    }
}
