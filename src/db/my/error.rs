use std::fmt;

#[derive(Debug, Clone)]
pub enum MyError {
    Generic(String),
    Refinery(String),
    FromRow,
}

impl fmt::Display for MyError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "MyError")
    }
}

impl std::error::Error for MyError {}

impl From<mysql_async::Error> for MyError {
    fn from(err: mysql_async::Error) -> Self {
        MyError::Generic(err.to_string())
    }
}

impl From<mysql::Error> for MyError {
    fn from(err: mysql::Error) -> Self {
        MyError::Generic(err.to_string())
    }
}

impl From<refinery::Error> for MyError {
    fn from(err: refinery::Error) -> Self {
        MyError::Refinery(err.to_string())
    }
}

