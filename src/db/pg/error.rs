use std::fmt;

#[derive(Debug, Clone)]
pub enum PgError {
    Pool,
    Refinery,
    Build,
    Format,
    Generic,
}

impl fmt::Display for PgError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "PgError")
    }
}

impl std::error::Error for PgError {}

impl From<tokio_postgres::Error> for PgError {
    fn from(_: tokio_postgres::Error) -> Self {
        PgError::Generic
    }
}

impl From<deadpool_postgres::BuildError> for PgError {
    fn from(_: deadpool_postgres::BuildError) -> Self {
        PgError::Build
    }
}

impl From<deadpool_postgres::PoolError> for PgError {
    fn from(_: deadpool_postgres::PoolError) -> Self {
        PgError::Pool
    }
}

impl From<refinery::error::Error> for PgError {
    fn from(_: refinery::error::Error) -> Self {
        PgError::Refinery
    }
}

impl From<std::fmt::Error> for PgError {
    fn from(_: std::fmt::Error) -> Self {
        PgError::Format
    }
}
