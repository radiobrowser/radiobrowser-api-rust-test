// Docs: https://docs.rs/tokio-postgres/latest/tokio_postgres/index.html

mod error;

use crate::db::Station;
use deadpool_postgres::{GenericClient, Manager, ManagerConfig, Pool, RecyclingMethod};
pub use error::PgError;
use futures::{pin_mut, TryStreamExt};
use log::{info, trace};
use postgres::Client;
use std::str::FromStr;
use tokio_postgres::{NoTls, Row};
use uuid::Uuid;

const TABLE_STATION_NAME: &'static str = "station";
const TABLE_STATION_COLUMNS: &'static str = "stationuuid,changeuuid,name,url,homepage,favicon,countrycode,state,language,tags,geo_lat,geo_long";

const TABLE_STATIONHISTORY_NAME: &'static str = "stationhistory";
const TABLE_STATIONHISTORY_COLUMNS: &'static str = "stationuuid,changeuuid,name,url,homepage,favicon,countrycode,state,language,tags,geo_lat,geo_long";

use super::{RemoteServer, StationClick, StationHistory, PG};

impl PG {
    pub fn new<P: AsRef<str>>(url: P, poolsize: usize) -> Result<Self, PgError> {
        info!("Init postgres connection");
        let pg_config = tokio_postgres::Config::from_str(url.as_ref())?;
        let mgr_config = ManagerConfig {
            recycling_method: RecyclingMethod::Fast,
        };
        let mgr = Manager::from_config(pg_config, NoTls, mgr_config);
        let pool = Pool::builder(mgr).max_size(poolsize).build()?;

        Ok(PG { pool })
    }

    fn station_row_to_struct(&self, row: Row) -> Station {
        let stationuuid = row.get(0);
        let changeuuid = row.get(1);
        let name = row.get(2);
        let url = row.get(3);
        let homepage = row.get(4);
        let favicon = row.get(5);
        let countrycode = row.get(6);
        let state = row.get(7);
        let language = row.get(8);
        let tags = row.get(9);
        let geo_lat = row.get(10);
        let geo_long = row.get(11);
        Station {
            stationuuid,
            changeuuid,
            name,
            url,
            homepage,
            favicon,
            countrycode,
            state,
            language,
            tags,
            geo_lat,
            geo_long,
        }
    }

    fn stationhistory_row_to_struct(&self, row: Row) -> StationHistory {
        let stationuuid = row.get(0);
        let changeuuid = row.get(1);
        let name = row.get(2);
        let url = row.get(3);
        let homepage = row.get(4);
        let favicon = row.get(5);
        let countrycode = row.get(6);
        let state = row.get(7);
        let language = row.get(8);
        let tags = row.get(9);
        let geo_lat = row.get(10);
        let geo_long = row.get(11);
        StationHistory {
            stationuuid,
            changeuuid,
            name,
            url,
            homepage,
            favicon,
            countrycode,
            state,
            language,
            tags,
            geo_lat,
            geo_long,
            source: String::new(),
        }
    }

    pub async fn get_stations(&self, offset: u64, limit: u64) -> Result<Vec<Station>, PgError> {
        let query = format!(
            "SELECT {} FROM {} LIMIT {} OFFSET {}",
            TABLE_STATION_COLUMNS, TABLE_STATION_NAME, limit, offset
        );
        let client = self.pool.get().await?;
        let stmt = client.prepare_cached(&query).await?;
        let rows = client.query(&stmt, &[]).await?;
        let list = rows
            .into_iter()
            .map(|row| self.station_row_to_struct(row))
            .collect();
        Ok(list)
    }

    pub async fn get_changes_by_uuid(
        &self,
        changeuuids: &[String],
    ) -> Result<Vec<StationHistory>, PgError> {
        trace!("get_changes_by_uuid() count={}", changeuuids.len());
        let client = self.pool.get().await?;
        let mut list = vec![];
        for chunk in changeuuids.chunks(100) {
            let in_items = (1..=chunk.len())
                .map(|item| format!("${}", item))
                .collect::<Vec<String>>()
                .join(",");
            let rows = client
                .query_raw(
                    &format!(
                        r#"SELECT {} FROM {} WHERE changeuuid IN ({})"#,
                        TABLE_STATIONHISTORY_COLUMNS, TABLE_STATIONHISTORY_NAME, in_items
                    ),
                    chunk,
                )
                .await?;
            pin_mut!(rows);
            while let Some(row) = rows.try_next().await? {
                list.push(self.stationhistory_row_to_struct(row));
            }
        }

        Ok(list)
    }

    pub async fn get_stations_by_uuid(
        &self,
        stationuuids: &[String],
    ) -> Result<Vec<Station>, PgError> {
        trace!("get_stations_by_uuid() count={}", stationuuids.len());
        let client = self.pool.get().await?;
        let mut list = vec![];
        for chunk in stationuuids.chunks(100) {
            let in_items = (1..=chunk.len())
                .map(|item| format!("${}", item))
                .collect::<Vec<String>>()
                .join(",");
            let rows = client
                .query_raw(
                    &format!(
                        r#"SELECT {} FROM {} WHERE stationuuid IN ({})"#,
                        TABLE_STATION_COLUMNS, TABLE_STATION_NAME, in_items
                    ),
                    chunk,
                )
                .await?;
            pin_mut!(rows);
            while let Some(row) = rows.try_next().await? {
                list.push(self.station_row_to_struct(row));
            }
        }

        Ok(list)
    }

    pub async fn insert_changes_raw(&self, changes: &[StationHistory]) -> Result<(), PgError> {
        trace!("insert_changes_raw() count={}", changes.len());
        let client = self.pool.get().await?;
        let stmt = client
            .prepare_cached(
                &format!("INSERT INTO {}(stationuuid,changeuuid,name,url,homepage,favicon,countrycode,
                    state,language,tags,geo_lat,geo_long) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)", TABLE_STATIONHISTORY_NAME),
            )
            .await?;
        for change in changes {
            client
                .query(
                    &stmt,
                    &[
                        &change.stationuuid,
                        &change.changeuuid,
                        &change.name,
                        &change.url,
                        &change.homepage,
                        &change.favicon,
                        &change.countrycode,
                        &change.state,
                        &change.language,
                        &change.tags,
                        &change.geo_lat,
                        &change.geo_long,
                    ],
                )
                .await?;
        }
        Ok(())
    }

    pub async fn insert_stations_raw(&self, stations: &[Station]) -> Result<(), PgError> {
        trace!("insert_stations_raw() len={}", stations.len());
        let client = self.pool.get().await?;
        let stmt = client
            .prepare_cached(
                "INSERT INTO station(stationuuid,changeuuid,name,url,homepage,favicon,countrycode,
                    state,language,tags,geo_lat,geo_long) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)",
            )
            .await?;
        for station in stations {
            client
                .query(
                    &stmt,
                    &[
                        &station.stationuuid,
                        &station.changeuuid,
                        &station.name,
                        &station.url,
                        &station.homepage,
                        &station.favicon,
                        &station.countrycode,
                        &station.state,
                        &station.language,
                        &station.tags,
                        &station.geo_lat,
                        &station.geo_long,
                    ],
                )
                .await?;
        }
        Ok(())
    }

    pub fn migrate<U>(url: U) -> Result<(), PgError>
    where
        U: AsRef<str>,
    {
        info!("Running DB migrations...");
        let mut client = Client::connect(url.as_ref(), NoTls)?;
        mod embedded {
            use refinery::embed_migrations;
            embed_migrations!("./migrations/pg");
        }
        let migration_report = embedded::migrations::runner().run(&mut client)?;
        for migration in migration_report.applied_migrations() {
            info!(
                "Migration Applied -  Name: {}, Version: {}",
                migration.name(),
                migration.version()
            );
        }

        info!("DB migrations finished!");

        Ok(())
    }

    pub async fn update_stations_raw(&self, stations: &[Station]) -> Result<(), PgError> {
        let client = self.pool.get().await?;
        let stmt = client
            .prepare_cached(
                r"UPDATE station SET changeuuid=$2,name=$3,
        url=$4,homepage=$5,favicon=$6,countrycode=$7,state=$8,
        language=$9,tags=$10,geo_lat=$11,geo_long=$12
        WHERE stationuuid=$1",
            )
            .await?;
        for station in stations.iter() {
            client
                .execute(
                    &stmt,
                    &[
                        &station.stationuuid,
                        &station.changeuuid,
                        &station.name,
                        &station.url,
                        &station.homepage,
                        &station.favicon,
                        &station.countrycode,
                        &station.state,
                        &station.language,
                        &station.tags,
                        &station.geo_lat,
                        &station.geo_long,
                    ],
                )
                .await?;
        }
        Ok(())
    }

    pub async fn ping(&self) -> Result<(), PgError> {
        let client = self.pool.get().await?;
        let stmt = client.prepare_cached(r"SELECT 1").await?;
        client.query(&stmt, &[]).await?;
        Ok(())
    }

    pub async fn get_remote_pull_server_list(&self) -> Result<Vec<RemoteServer>, PgError> {
        let client = self.pool.get().await?;
        let stmt = client
            .prepare_cached("SELECT name,last_station_uuid FROM remoteserver")
            .await?;
        let rows = client.query(&stmt, &[]).await?;
        let mut list = vec![];
        for row in rows {
            let name = row.get(0);
            let last_station_uuid = row.get(1);
            list.push(RemoteServer {
                name,
                last_station_uuid,
            });
        }
        Ok(list)
    }

    pub async fn update_remote_pull_server_lastuuid<P: AsRef<str>, Q: AsRef<str>>(
        &self,
        servername: P,
        lastuuid: Q,
    ) -> Result<(), PgError> {
        let client = self.pool.get().await?;
        let stmt = client
            .prepare_cached("UPDATE remoteserver SET last_station_uuid=$2 WHERE name=$1")
            .await?;
        client
            .query(
                &stmt,
                &[
                    &servername.as_ref().to_string(),
                    &lastuuid.as_ref().to_string(),
                ],
            )
            .await?;
        Ok(())
    }

    pub async fn get_remote_pull_server<P: AsRef<str>>(
        &self,
        servername: P,
    ) -> Result<Option<RemoteServer>, PgError> {
        let client = self.pool.get().await?;
        let stmt = client
            .prepare_cached("SELECT name,last_station_uuid FROM remoteserver WHERE name=$1")
            .await?;
        let rows = client
            .query(&stmt, &[&servername.as_ref().to_string()])
            .await?;
        for row in rows {
            let name = row.get(0);
            let last_station_uuid = row.get(1);
            return Ok(Some(RemoteServer {
                name,
                last_station_uuid,
            }));
        }

        Ok(None)
    }
    /* Count stuff */
    pub async fn get_station_count(&self) -> Result<i64, PgError> {
        let client = self.pool.get().await?;
        let stmt = client
            .prepare_cached(r"SELECT COUNT(*) FROM station")
            .await?;
        let row = client.query_one(&stmt, &[]).await?;
        Ok(row.get(0))
    }

    pub async fn insert_station_click<P: AsRef<str>>(
        &self,
        stationuuid: Uuid,
        ip: P,
    ) -> Result<(), PgError> {
        trace!("insert_station_click({}, {})", stationuuid, ip.as_ref());

        let client = self.pool.get().await?;
        let query = "INSERT INTO stationclick(stationuuid, clickuuid, ip, clicktime) VALUES($1,$2,$3,timezone('utc', now()))";
        client
            .execute(
                query,
                &[
                    &&stationuuid.to_string(),
                    &Uuid::new_v4().to_string(),
                    &ip.as_ref(),
                ],
            )
            .await?;
        Ok(())
    }

    pub async fn get_station_click(
        &self,
        stationuuid: Option<Uuid>,
    ) -> Result<Vec<StationClick>, PgError> {
        trace!("get_station_click({:?})", stationuuid);
        let client = self.pool.get().await?;

        let list: Vec<Row> = match stationuuid {
            Some(stationuuid) => {
                client
                .query(
                    "SELECT stationuuid,clickuuid,ip FROM stationclick WHERE stationuuid=$1 ORDER BY clicktime",
                        &[&stationuuid.to_string()]
                )
                .await?
            }
            None => {
                client
                .query(
                    "SELECT stationuuid,clickuuid,ip FROM stationclick ORDER BY clicktime",
                    &[],
                )
                .await?
            }
        };
        let resultlist = list
            .into_iter()
            .map(|item| StationClick {
                stationuuid: item.get(0),
                clickuuid: item.get(1),
                ip: item.get(2),
            })
            .collect();
        Ok(resultlist)
    }
}
