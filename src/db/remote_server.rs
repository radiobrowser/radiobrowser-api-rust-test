use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct RemoteServer {
    pub name: String,
    pub last_station_uuid: Option<String>,
}
