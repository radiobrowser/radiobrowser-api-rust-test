// Docs: https://docs.rs/mongodb/latest/mongodb/index.html

mod error;

use crate::db::Station;
pub use error::MongoError;
use futures::TryStreamExt;
use log::{info, trace};
use mongodb::{
    bson::{doc, Document},
    options::{FindOptions, InsertManyOptions, UpdateOptions},
};
use uuid::Uuid;

use super::{Mongo, RemoteServer, StationClick, StationHistory};
const COLLECTION_STATION: &'static str = "station";
const COLLECTION_STATIONCLICK: &'static str = "stationclick";

impl Mongo {
    pub async fn new<P: AsRef<str>>(url: P) -> Result<Self, MongoError> {
        info!("Init mongodb connection");
        use mongodb::{options::ClientOptions, Client};

        // Parse a connection string into an options struct.
        let mut client_options = ClientOptions::parse(url.as_ref()).await?;

        // Manually set an option.
        client_options.app_name = Some("RadioBrowserAPI".to_string());

        // Get a handle to the deployment.
        let client = Client::with_options(client_options)?;

        Ok(Mongo { client })
    }

    pub async fn get_stations(&self, offset: u64, limit: u64) -> Result<Vec<Station>, MongoError> {
        let limit: i64 = limit as i64;
        let db = self
            .client
            .default_database()
            .ok_or(MongoError::NoDefaultDB)?;
        let collection = db.collection::<Station>(COLLECTION_STATION);

        // Query the books in the collection with a filter and an option.
        let filter = doc! {};
        let find_options = FindOptions::builder()
            .limit(Some(limit))
            .skip(offset)
            //.sort(doc! { "name": 1 })
            .build();
        let mut cursor = collection.find(filter, find_options).await?;

        // Iterate over the results of the cursor.
        let mut list = vec![];
        while let Some(station) = cursor.try_next().await? {
            list.push(station);
        }
        Ok(list)
    }

    pub async fn get_changes_by_uuid(
        &self,
        changeuuids: &[String],
    ) -> Result<Vec<StationHistory>, MongoError> {
        trace!("get_changes_by_uuid() count={}", changeuuids.len());
        let db = self
            .client
            .default_database()
            .ok_or(MongoError::NoDefaultDB)?;
        let collection = db.collection::<StationHistory>("stationhistory");
        let result = collection
            .find(
                doc! {
                    "_id": {
                        "$in":changeuuids
                    }
                },
                None,
            )
            .await?;
        Ok(result.try_collect().await?)
    }

    pub async fn get_stations_by_uuid(
        &self,
        stationuuids: &[String],
    ) -> Result<Vec<Station>, MongoError> {
        trace!("get_stations_by_uuid() count={}", stationuuids.len());
        let db = self
            .client
            .default_database()
            .ok_or(MongoError::NoDefaultDB)?;
        let collection = db.collection::<Station>(COLLECTION_STATION);
        let result = collection
            .find(
                doc! {
                    "_id": {
                        "$in":stationuuids
                    }
                },
                None,
            )
            .await?;
        Ok(result.try_collect().await?)
    }

    pub async fn insert_changes_raw(&self, changes: &[StationHistory]) -> Result<(), MongoError> {
        trace!("insert_changes_raw() count={}", changes.len());
        let db = self
            .client
            .default_database()
            .ok_or(MongoError::NoDefaultDB)?;
        let collection = db.collection::<Document>("stationhistory");
        let docs: Vec<Document> = changes
            .iter()
            .map(|stationhistory| stationhistory_to_doc(stationhistory))
            .collect();

        if docs.len() > 0 {
            let options = InsertManyOptions::builder().ordered(false).build();
            collection.insert_many(docs, options).await?;
        }

        Ok(())
    }

    pub async fn insert_stations_raw(&self, stations: &[Station]) -> Result<(), MongoError> {
        trace!("insert_stations_raw() count={}", stations.len());

        let db = self
            .client
            .default_database()
            .ok_or(MongoError::NoDefaultDB)?;
        let collection = db.collection::<Document>(COLLECTION_STATION);
        if stations.len() > 0 {
            let docs: Vec<Document> = stations
                .into_iter()
                .map(|station| station_to_doc(station))
                .collect();

            let options = InsertManyOptions::builder().ordered(false).build();
            collection.insert_many(docs, options).await?;
        }

        Ok(())
    }

    pub async fn update_stations_raw(&self, stations: &[Station]) -> Result<(), MongoError> {
        let db = self
            .client
            .default_database()
            .ok_or(MongoError::NoDefaultDB)?;
        let collection = db.collection::<Document>(COLLECTION_STATION);
        for single in stations.into_iter() {
            collection
                .update_one(
                    doc! {"_id": single.stationuuid.to_string()},
                    station_to_doc(&single),
                    None,
                )
                .await?;
        }
        Ok(())
    }

    pub fn migrate<U>(_url: U) -> Result<(), MongoError>
    where
        U: AsRef<str>,
    {
        info!("Running DB migrations... NOT NEEDED");

        Ok(())
    }

    pub async fn ping(&self) -> Result<(), MongoError> {
        let db = self
            .client
            .default_database()
            .ok_or(MongoError::NoDefaultDB)?;

        db.run_command(doc! {"ping": 1}, None).await?;
        Ok(())
    }

    pub async fn get_station_count(&self) -> Result<u64, MongoError> {
        let db = self
            .client
            .default_database()
            .ok_or(MongoError::NoDefaultDB)?;
        let collection = db.collection::<Document>(COLLECTION_STATION);
        let count = collection.count_documents(None, None).await?;
        Ok(count)
    }

    pub async fn get_remote_pull_server_list(&self) -> Result<Vec<RemoteServer>, MongoError> {
        let db = self
            .client
            .default_database()
            .ok_or(MongoError::NoDefaultDB)?;
        let collection = db.collection::<RemoteServer>("remote_servers");
        let mut cursor = collection.find(None, None).await?;
        let mut list = vec![];
        while let Some(station) = cursor.try_next().await? {
            list.push(station);
        }
        Ok(list)
    }

    pub async fn update_remote_pull_server_lastuuid<P: AsRef<str>, Q: AsRef<str>>(
        &self,
        servername: P,
        lastuuid: Q,
    ) -> Result<(), MongoError> {
        let db = self
            .client
            .default_database()
            .ok_or(MongoError::NoDefaultDB)?;
        let collection = db.collection::<RemoteServer>("remote_servers");

        let filter = doc! { "name": servername.as_ref() };
        let options = UpdateOptions::builder().upsert(true).build();

        let update = doc! {"$set": {
            "name": servername.as_ref(),
            "last_station_uuid": lastuuid.as_ref(),
        }};
        collection.update_one(filter, update, options).await?;
        Ok(())
    }

    pub async fn get_remote_pull_server<P: AsRef<str>>(
        &self,
        servername: P,
    ) -> Result<Option<RemoteServer>, MongoError> {
        let db = self
            .client
            .default_database()
            .ok_or(MongoError::NoDefaultDB)?;
        let collection = db.collection::<RemoteServer>("remote_servers");
        let filter = doc! { "name": servername.as_ref() };
        let item = collection.find_one(filter, None).await?;
        Ok(item)
    }

    pub async fn insert_station_click<P: AsRef<str>>(
        &self,
        stationuuid: Uuid,
        ip: P,
    ) -> Result<(), MongoError> {
        trace!("insert_station_click({}, {})", stationuuid, ip.as_ref());

        let db = self
            .client
            .default_database()
            .ok_or(MongoError::NoDefaultDB)?;
        let collection = db.collection::<Document>(COLLECTION_STATIONCLICK);
        collection
            .insert_one(
                doc! {
                    "stationuuid": stationuuid.to_string(),
                    "clickuuid": Uuid::new_v4().to_string(),
                    "ip": ip.as_ref(),
                    "$currentDate": {
                        "clicktime": true,
                    },
                },
                None,
            )
            .await?;
        Ok(())
    }

    pub async fn get_station_click(
        &self,
        stationuuid: Option<Uuid>,
    ) -> Result<Vec<StationClick>, MongoError> {
        trace!("get_station_click({:?})", stationuuid);
        let db = self
            .client
            .default_database()
            .ok_or(MongoError::NoDefaultDB)?;
        let collection = db.collection::<StationClick>(COLLECTION_STATIONCLICK);
        let filter = match stationuuid {
            Some(stationuuid) => {
                doc! {
                    "stationuuid": stationuuid.to_string()
                }
            }
            None => {
                doc! {}
            }
        };
        Ok(collection.find(filter, None).await?.try_collect().await?)
    }
}

fn station_to_doc(station: &Station) -> Document {
    doc! {
        "_id": &station.stationuuid,
        "stationuuid": &station.stationuuid,
        "changeuuid": &station.changeuuid,
        "name": &station.name,
        "url": &station.url,
        "homepage": &station.homepage,
        "favicon": &station.favicon,
        "countrycode": &station.countrycode,
        "state": &station.state,
        "language": &station.language,
        "tags": &station.tags,
        "geo_lat": &station.geo_lat,
        "geo_long": &station.geo_long,
        "loc": {
            "type": "Point",
            "coordinates": [ station.geo_long, station.geo_lat ]
        },
    }
}

fn stationhistory_to_doc(stationhistory: &StationHistory) -> Document {
    doc! {
        "_id": &stationhistory.changeuuid,
        "stationuuid": &stationhistory.stationuuid,
        "changeuuid": &stationhistory.changeuuid,
        "name": &stationhistory.name,
        "url": &stationhistory.url,
        "homepage": &stationhistory.homepage,
        "favicon": &stationhistory.favicon,
        "countrycode": &stationhistory.countrycode,
        "state": &stationhistory.state,
        "language": &stationhistory.language,
        "tags": &stationhistory.tags,
        "geo_lat": &stationhistory.geo_lat,
        "geo_long": &stationhistory.geo_long,
        "source": &stationhistory.source,
    }
}
