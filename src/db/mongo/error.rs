use std::fmt;

#[derive(Debug, Clone)]
pub enum MongoError {
    Generic,
    NoDefaultDB,
}

impl fmt::Display for MongoError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "MongoError")
    }
}

impl std::error::Error for MongoError {}

impl From<mongodb::error::Error> for MongoError {
    fn from(x: mongodb::error::Error) -> Self {
        println!("{:?}", x);
        MongoError::Generic
    }
}
