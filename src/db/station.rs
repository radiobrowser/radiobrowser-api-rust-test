use serde::{Deserialize, Serialize};
use uuid::Uuid;

use super::StationHistory;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Station {
    pub stationuuid: String,
    pub changeuuid: String,
    pub name: String,
    pub url: String,
    pub homepage: String,
    pub favicon: String,
    pub countrycode: String,
    pub state: String,
    pub language: String,
    pub tags: String,
    pub geo_lat: Option<f64>,
    pub geo_long: Option<f64>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NewStation {
    pub name: String,
    pub url: String,
    pub homepage: String,
    pub favicon: String,
    pub countrycode: String,
    pub state: String,
    pub language: String,
    pub tags: String,
    pub geo_lat: Option<f64>,
    pub geo_long: Option<f64>,
}

impl From<NewStation> for Station {
    fn from(value: NewStation) -> Self {
        Station {
            stationuuid: Uuid::new_v4().to_string(),
            changeuuid: Uuid::new_v4().to_string(),
            name: value.name,
            url: value.url,
            homepage: value.homepage,
            favicon: value.favicon,
            countrycode: value.countrycode,
            state: value.state,
            language: value.language,
            tags: value.tags,
            geo_lat: value.geo_lat,
            geo_long: value.geo_long,
        }
    }
}

impl From<StationHistory> for Station {
    fn from(value: StationHistory) -> Self {
        Station {
            stationuuid: value.stationuuid,
            changeuuid: value.changeuuid,
            name: value.name,
            url: value.url,
            homepage: value.homepage,
            favicon: value.favicon,
            countrycode: value.countrycode,
            state: value.state,
            language: value.language,
            tags: value.tags,
            geo_lat: value.geo_lat,
            geo_long: value.geo_long,
        }
    }
}
