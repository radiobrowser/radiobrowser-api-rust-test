use std::fmt;

use super::mongo::MongoError;
use super::my::MyError;
use super::pg::PgError;

#[derive(Debug, Clone)]
pub enum DbError {
    My(MyError),
    Pg(PgError),
    Mongo(MongoError),
    UnknownUrl,
}

impl fmt::Display for DbError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "DbError")
    }
}

impl std::error::Error for DbError {}

impl From<MyError> for DbError {
    fn from(err: MyError) -> Self {
        DbError::My(err)
    }
}

impl From<PgError> for DbError {
    fn from(err: PgError) -> Self {
        DbError::Pg(err)
    }
}

impl From<MongoError> for DbError {
    fn from(err: MongoError) -> Self {
        DbError::Mongo(err)
    }
}
